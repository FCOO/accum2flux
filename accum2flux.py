#!/usr/bin/env python
import optparse
import numpy as np
import netCDF4 as netCDF

#
# Programmers
#  2014-12-02:BJB: Simple script based on nco/ncap2 script
#  2018-03-05:BJB: Add maxlimit to avoid overflow/wrapping.
#  2021-01-19:BJB: Adapt to python3 (@datafetch02)
#  2024-04-11:BRS: Update to numpy 1.20+ (np.float deprecated)

def main():
    """\
    Re-compute a accumulative variable to be a time-flux.
    For the first and list slice, one-sided differences are 
    used (forward and backward, respectively, while central 
    differences are used for the "internal" time slices
    
    WARNING: Overwrites input file.
    """
    usage = "usage: %prog [options] filename"
    parser = optparse.OptionParser(usage=usage)
    parser.add_option('--varnames', dest='varnames', action='store',
      type='str', default='',
      help='Variable (commma separated list) to work on.')
    parser.add_option('--multfactor', dest='multfactor', action='store',
      type='str', default='',
      help='Constant to multiply to all variable elements as part of convert'+
                      ' (commma separated list accepted - same length as varnames)')
    parser.add_option('--maxlimit', dest='maxlimit', action='store',
      type='str', default='',
      help='Impose a maximum limit on output. All values exceeding the limit will be reset to the max value.')
    (options, args) = parser.parse_args()

    if len(args) != 1:
      parser.error("incorrect number of arguments")

    # Get list of files to work on:
    varstr = options.varnames
    if (varstr == ''):
        print("No varnames given")
        exit(1)
    # Make a list of the variables, which should be modified (likely it is only one)
    varnames = varstr.split(',')

    multfactors = np.ones(shape=(len(varnames),),dtype=float)
    mltstring=options.multfactor
    if (mltstring == ''):
        print("No multiplication factors given - will use unity")
    else:
        mltstrs=mltstring.split(',') # Now list of strings
        if len(mltstrs)==1:
            # One scale (for all variables)
            multfactors[:] = float(mltstrs[0])
        else:
            # One scale PER varible
            multfactors = np.array(map(float,mltstring.split(',')))
            if multfactors.size != len(varnames):
                print("Numbers of vars (varnames) and scales (multfactor) does not match")
                exit(1)
    #
    maxvalstr=options.maxlimit
    if (maxvalstr == ''):
        print("No maxlimit given. Will not impose maximum on output")
    else:
        maxval=float(maxvalstr)

    # Netcdf file, which we will modify:
    filename = args[0]
    # Open file for modification:
    print("Open netcdf file %s"%filename)
    nc     = netCDF.Dataset(filename, 'a')
    # Simple sanity checks:
    if( 'time' not in nc.dimensions ):
        print("NC-file %s does not include time dimension"%filename)
        exit(1)
    if( 'time' not in nc.variables ):
        print("NC-file %s does not include time variable"%filename)
        exit(1)
    if(nc.variables['time'].size<2):
        print("NC-file %s time length < 2. Cannot comput flux."%filename)
        exit(1)
    print("Read time values")
    # Go over variables and make sure that they have expected name, shape etc.
    print("Pre-check variables")
    for varnam in (varnames):
        print(" ... check %s"%varnam)
        # MUST exist in file:
        if ( varnam not in nc.variables):
            print("NC-file %s does not include %s variable"%(filename,varnam))
            exit(1)
        # Shorthand:
        ncvar = nc.variables[varnam]
        # MUST have at least one dimension:
        if (len(ncvar.dimensions)!=3):
            print("Variable %s not spanned by three dimensions (hardcoding) - cannot compute fluxes"%varnam)
            exit(1)
        # First dimension MUST be time:
        if (ncvar.dimensions[0] != 'time'):
            print("Variable %s not spanned by time dimension - cannot compute fluxes"%varnam)
            exit(1)

    # Put time variable into well-defined np-array:
    nctim      = nc.variables['time']
    ntim       = len(nc.dimensions['time'])
    timdata    = np.empty(shape=(ntim,),dtype=nctim.dtype)
    timdata[:] = nctim[:]

    print("Convert accumulations to fluxes")
    ivar=-1
    for varnam in (varnames):
        ivar += 1
        print(" Convert %s"%varnam)
        ncvar = nc.variables[varnam]
        dims = ncvar.dimensions
        dimsizes = list(range(len(dims)))
        for idim in range(len(dims)):
            dimnam = dims[idim]
            dimlen = len(nc.dimensions[dimnam])
            dimsizes[idim]=dimlen
        print("Allocating data for read/write:",dimsizes)
        accumdata = np.empty(shape=dimsizes,dtype=ncvar.dtype)
        fluxdata  = np.zeros(shape=dimsizes,dtype=ncvar.dtype)
        # TODO: The above allocations of (itim,nx,ny) can be replaced by 
        #  a more general (itime,nslap), where nslap=dimsizes[1:].prod()
        #  (Note that this product-method happens to give unity if 
        #  only time spans the variable)
        #  This approach should make it possible to work on variables with
        #  a general number of dimensions. We need then to use 
        #  np.reshape to go back and forth between the sizes (on read and 
        #  on write).
        #
        # In principle, it is possible to interlace the read with the compute here.
        # For now we just read the entire blob
        print('  reading %s data values'%varnam)
        accumdata[:,:,:]=ncvar[:,:,:]

        #for itim in range(ntim):
        #    accumdata[itim,:,:]=ncvar[itim,:].reshape(slap_size)
        print(' compute simple first-order difference at start')
        itim=0
        scale             = multfactors[ivar]/(timdata[itim+1]-timdata[itim])
        #  TODO: Should we cast scale to same dtype as accumdata?
        fluxdata[itim,:,:]= scale*(accumdata[itim+1,:,:]-accumdata[itim,:,:])
        print(' compute central second-order differences')
        for itim in range(1,ntim-1):
            scale             = multfactors[ivar]/(timdata[itim+1]-timdata[itim-1])
            fluxdata[itim,:,:]= scale*(accumdata[itim+1,:,:]-accumdata[itim-1,:,:])
        print(' compute simple first-order difference at end')
        itim=ntim-1
        scale             = multfactors[ivar]/(timdata[itim]-timdata[itim-1])
        fluxdata[itim,:,:]= scale*(accumdata[itim,:,:]-accumdata[itim-1,:,:])
        print(' fix rounding negatives')
        fluxdata[fluxdata<0.]=0.
        if (maxvalstr != ''):
            # Get previous max:
            thismax=fluxdata.max()
            if (thismax > maxval):
                print(' replacing computed maximum (%f) with imposed/hard max value (%f)'%(thismax,maxval))
                fluxdata[fluxdata>maxval]=maxval
            else:
                print(' computed max precipitation (%f) is smaller than hard max value (%f). No replacement necessary' %(thismax,maxval))
        #
        # Do store
        print("  store %s data"%varnam)
        ncvar[:,:,:] = fluxdata[:,:,:]

    print("Close %s"%filename)
    nc.close()
    print("All done")
    # End of main.

if __name__ == '__main__':
    exit(main())
